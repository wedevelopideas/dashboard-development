<?php

return [
    'bitbucket' => 'BitBucket',
    'comparing_changes' => 'Comparing changes',
    'email' => 'Email',
    'github' => 'GitHub',
    'login' => 'Login',
    'logout' => 'Logout',
    'password' => 'Password',
    'projects' => 'Projects',
    'repositories' => 'Repositories',
    'tools' => 'Tools',
    'users' => 'Users',
];
