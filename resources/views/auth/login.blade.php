@extends('layouts.blank')
@section('title', 'dashboard')
@section('content')
    <div class="ui middle aligned center aligned grid">
        <div class="column">
            <h2 class="ui inverted header">
                dashboard
            </h2>
            <form action="{{ route('login') }}" method="post" class="ui large form">
                @csrf

                <div class="ui stacked segment">
                    <div class="field{{ $errors->has('email') ? ' error' : '' }}">
                        <div class="ui left icon input">
                            <i class="user icon"></i>
                            <input type="text" name="email" autocomplete="off" placeholder="{{ trans('common.email') }}">
                        </div>
                    </div>
                    <div class="field{{ $errors->has('password') ? ' error' : '' }}">
                        <div class="ui left icon input">
                            <i class="lock icon"></i>
                            <input type="password" name="password" autocomplete="off" placeholder="{{ trans('common.password') }}">
                        </div>
                    </div>
                    <button type="submit" class="ui fluid large green submit button">{{ trans('common.login') }}</button>
                </div>
            </form>
        </div>
    </div>
@endsection