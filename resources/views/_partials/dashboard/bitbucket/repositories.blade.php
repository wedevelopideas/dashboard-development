<div class="ui inverted segment" style="min-height: 328px;">
    <h4 class="ui inverted header">
        <i class="bitbucket icon"></i>
        <span class="content">
            BitBucket
            <small>
                {{ trans('common.repositories') }}
            </small>
        </span>
    </h4>
    <div class="ui inverted middle aligned list">
        @foreach($repositories as $repository)
            <div class="item">
                <i class="large bitbucket middle aligned icon"></i>
                <div class="content">
                    <a href="{{ $repository->html_url }}" target="_blank" rel="noreferrer" class="header">
                        {{ $repository->full_name }}
                    </a>
                </div>
            </div>
        @endforeach
    </div>
</div>