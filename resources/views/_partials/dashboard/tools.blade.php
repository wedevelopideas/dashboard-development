<div class="ui inverted segment" style="min-height: 328px;">
    <h4 class="ui inverted header">
        <i class="github icon"></i>
        <span class="content">
            {{ trans('common.tools') }}
        </span>
    </h4>
    <div class="ui inverted middle aligned list">
        @foreach($tools as $tool)
            <div class="item">
                <i class="large github middle aligned icon"></i>
                <div class="content">
                    <a href="{{ $tool->html_url }}" target="_blank" rel="noreferrer" class="header">
                        {{ $tool->full_name }}
                        <div class="sub header">
                            {{ $tool->tag_name }}
                        </div>
                    </a>
                </div>
            </div>
        @endforeach
    </div>
    <h4 class="ui inverted header">
        <i class="github icon"></i>
        <span class="content">
            {{ trans('common.comparing_changes') }}
        </span>
    </h4>
    <div class="ui inverted middle aligned list">
        <div class="item">
            <div class="content">
                <a href="https://github.com/laravel/laravel/compare/5.7...master" target="_blank" rel="noreferrer" class="header">
                    Laravel
                </a>
            </div>
        </div>
    </div>
</div>