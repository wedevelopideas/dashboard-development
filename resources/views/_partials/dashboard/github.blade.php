<div class="ui inverted segment" style="min-height: 328px;">
    <h4 class="ui inverted header">
        <i class="github icon"></i>
        <span class="content">
            GitHub
            <small>{{ trans('common.users') }}</small>
        </span>
    </h4>
    <div class="ui inverted middle aligned list">
        @foreach($followings as $following)
            <div class="item">
                <img src="{{ $following->avatar_url }}" alt="{{ $following->username }}" class="ui avatar image">
                <div class="content">
                    <a href="{{ $following->html_url }}" target="_blank" rel="noreferrer" class="header">
                        {{ $following->GitHubName() }}
                        <div class="description">{{ $following->location }}</div>
                    </a>
                </div>
            </div>
        @endforeach
    </div>
</div>