<div class="ui inverted center aligned segment">
    <div class="ui medium image">
        <img src="{{ asset(auth()->user()->avatar) }}" alt="{{ auth()->user()->display_name }}">
        @if (auth()->user()->providers->count() > 0)
            <div class="ui attached buttons">
                @foreach(auth()->user()->providers as $provider)
                    <a href="{{ route('login.'.$provider->provider) }}" class="ui button">
                        <i class="{{ $provider->provider }} icon"></i>
                    </a>
                @endforeach
            </div>
        @endif
    </div>
</div>