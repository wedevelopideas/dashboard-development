@extends('layouts.frontend')
@section('title', 'dashboard')
@section('content')
    <div class="ui main container">
        <div class="ui stackable grid">
            <div class="row">
                <div class="column">
                    <h1 class="ui inverted header">
                        <i class="code icon"></i>
                        <span class="content">
                            {{ $project->name }}
                        </span>
                    </h1>
                </div>
            </div>
            <div class="row">
                <div class="four wide column">
                    <div class="ui inverted segment">
                        <h3 class="ui header">
                            <i class="users icon"></i>
                            <span class="content">
                                {{ trans('common.users') }}
                            </span>
                        </h3>
                        <div class="ui inverted list">
                            @foreach($members as $member)
                                <div class="item">
                                    <img src="{{ asset($member->avatar) }}" alt="{{ $member->display_name }}"
                                         class="ui avatar image">
                                    <div class="content">
                                        <div class="header">
                                            {{ $member->display_name }}
                                        </div>
                                        {{ $member->type }}
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection