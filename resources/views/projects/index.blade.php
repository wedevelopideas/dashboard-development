@extends('layouts.frontend')
@section('title', 'dashboard')
@section('content')
    <div class="ui main fluid container">
        <div class="ui stackable grid">
            <div class="row">
                <div class="column">
                    <h1 class="ui inverted header">
                        <i class="code icon"></i>
                        <span class="content">
                            {{ trans('common.projects') }}
                        </span>
                    </h1>
                </div>
            </div>
            <div class="row">
                <div class="column">
                    <div class="ui cards">
                        @foreach($projects as $project)
                            <div class="card">
                                <div class="content">
                                    <a href="{{ route('projects.view', ['full_name' => $project->full_name]) }}" class="center aligned header">
                                        {{ $project->name }}
                                    </a>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection