@extends('layouts.frontend')
@section('title', 'dashboard')
@section('content')
    <div class="ui stackable grid">
        <div class="row">
            <div class="column">
                <h1 class="ui inverted header">
                    <i class="bitbucket icon"></i>
                    <span class="content">
                        BitBucket
                        <small>{{ trans('common.repositories') }}</small>
                    </span>
                </h1>
            </div>
        </div>
        <div class="row">
            <div class="column">
                <div class="ui cards">
                    @foreach($repositories as $repository)
                        <div class="card">
                            <div class="content">
                                <a href="{{ $repository->html_url }}" target="_blank" class="center aligned header">{{ $repository->name }}</a>
                                <div class="center aligned meta">
                                    <i class="time icon"></i> {{ \Carbon\Carbon::parse($repository->created_at)->diffForHumans() }}
                                    <i class="edit icon"></i> {{ \Carbon\Carbon::parse($repository->updated_at)->diffForHumans() }}
                                </div>
                            </div>
                            <div class="extra content">
                                <div class="center aligned author">
                                    {{ $repository->name }}
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
@endsection