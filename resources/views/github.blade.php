@extends('layouts.frontend')
@section('title', 'dashboard')
@section('content')
    <div class="ui stackable grid">
        <div class="row">
            <div class="column">
                <h1 class="ui inverted header">
                    <i class="github icon"></i>
                    <span class="content">
                        GitHub
                        <small>{{ trans('common.users') }}</small>
                    </span>
                </h1>
            </div>
        </div>
        <div class="row">
            <div class="column">
                <div class="ui cards">
                    @foreach($users as $user)
                        <div class="card">
                            <div class="content">
                                <a href="{{ $user->html_url }}" target="_blank" rel="noreferrer" class="center aligned header">{{ $user->GitHubName() }}</a>
                                <div class="center aligned meta">
                                    <i class="user plus icon"></i> {{ \Carbon\Carbon::parse($user->created_at)->diffForHumans() }}
                                    <i class="edit icon"></i> {{ \Carbon\Carbon::parse($user->updated_at)->diffForHumans() }}
                                </div>
                            </div>
                            <div class="extra content">
                                <div class="center aligned author">
                                    <img src="{{ $user->avatar_url }}" alt="{{ $user->username }}" class="ui avatar image">
                                    {{ $user->GitHubName() }}
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
@endsection