@extends('layouts.frontend')
@section('title', 'dashboard')
@section('content')
    <div class="ui stackable grid">
        <div class="four wide column">
            @include('_partials.dashboard.user')
        </div>
        @if ($followings->count())
        <div class="four wide column">
            @include('_partials.dashboard.github')
        </div>
        @endif
        <div class="four wide column">
            @include('_partials.dashboard.bitbucket.repositories')
        </div>
        <div class="four wide column">
            @include('_partials.dashboard.tools')
        </div>
    </div>
@endsection