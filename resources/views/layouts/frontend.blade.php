<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, viewport-fit=cover">
    <title>@yield('title')</title>
    <meta name="description" content="personal Dashboard for GitHub and Bitbucket">
    <link rel="stylesheet" href="{{ asset('assets/semantic.min.css') }}">
    <style type="text/css">
        body {
            background-color: #000000;
        }
        .ui.menu .item img.logo {
            margin-right: 1.5em;
        }
        .main.container {
            margin-top: 7em;
        }
    </style>
</head>
<body>
<div class="ui fixed inverted menu">
    <div class="ui fluid container">
        <a href="{{ route('dashboard') }}" class="header item">
            dashboard
        </a>
        <a href="{{ route('github') }}" class="item">
            GitHub
        </a>
        <a href="{{ route('bitbucket') }}" class="item">
            BitBucket
        </a>
        <a href="{{ route('projects') }}" class="item">
            {{ trans('common.projects') }}
        </a>
        <div class="right menu">
            <a href="{{ route('user', ['user_name' => auth()->user()->user_name]) }}" class="item">
                {{ auth()->user()->first_name }}
            </a>
            <a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();" title="{{ trans('common.logout') }}" class="icon item">
                <i class="sign out icon"></i>
            </a>
            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                @csrf

            </form>
        </div>
    </div>
</div>

<div class="ui main fluid container">
    @yield('content')
</div>
</body>
</html>