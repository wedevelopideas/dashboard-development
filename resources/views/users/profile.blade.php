@extends('layouts.frontend')
@section('title', 'dashboard')
@section('content')
    <div class="ui main container">
        <div class="ui stackable grid">
            <div class="row">
                <div class="column">
                    <div class="ui medium image">
                        <img src="{{ asset($user->avatar) }}" alt="{{ $user->display_name }}">
                        <div class="ui bottom attached label">
                            {{ $user->display_name }}
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="column">
                    <div class="ui cards">
                        @foreach($accounts as $account)
                            <div class="card">
                                <div class="content">
                                    <img class="right floated mini ui image" src="{{ $account->avatar }}">
                                    <a href="{{ $account->html_url }}" target="_blank" class="header">
                                        {{ $account->name }}
                                    </a>
                                    <div class="meta">
                                        {{ trans('common.'.$account->provider) }}
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection