<?php

namespace Tests;

use App\dashboard\Users\Models\Users;
use App\dashboard\Projects\Models\Projects;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\TestCase as BaseTestCase;

abstract class TestCase extends BaseTestCase
{
    use CreatesApplication, DatabaseMigrations;

    /**
     * @var Projects
     */
    protected $project;

    /**
     * @var Users
     */
    protected $user;

    /**
     * Set up the test.
     */
    public function setUp(): void
    {
        parent::setUp();
        $this->project = factory(Projects::class)->create();
        $this->user = factory(Users::class)->create();
    }

    /**
     * Reset the migrations.
     */
    public function tearDown(): void
    {
        $this->artisan('migrate:reset');
        parent::tearDown();
    }
}
