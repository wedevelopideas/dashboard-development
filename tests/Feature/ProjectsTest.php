<?php

namespace Tests\Feature;

use Tests\TestCase;

class ProjectsTest extends TestCase
{
    /** @test */
    public function test_shows_all_projects()
    {
        $response = $this->actingAs($this->user)->get(route('projects'));
        $response->assertSuccessful();
    }

    /** @test */
    public function test_shows_project()
    {
        $response = $this->actingAs($this->user)->get(route('projects.view', ['full_name' => $this->project->full_name]));
        $response->assertSuccessful();
    }
}
