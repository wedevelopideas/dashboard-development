<?php

namespace Tests\Feature;

use Tests\TestCase;

class DashboardTest extends TestCase
{
    /** @test */
    public function test_shows_dashboard()
    {
        $response = $this->actingAs($this->user)->get(route('dashboard'));
        $response->assertSuccessful();
    }

    /** @test */
    public function test_redirects_user_to_login()
    {
        $response = $this->get(route('dashboard'));
        $response->assertRedirect(route('login'));
    }
}
