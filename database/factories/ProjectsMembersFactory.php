<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use Faker\Generator as Faker;
use App\dashboard\Users\Models\Users;
use App\dashboard\Projects\Models\Projects;
use App\dashboard\Projects\Models\ProjectsMembers;

$factory->define(ProjectsMembers::class, function (Faker $faker) {
    return [
        'project_id' => function () {
            return factory(Projects::class)->create()->id;
        },
        'user_id' => function () {
            return factory(Users::class)->create()->id;
        },
        'type' => $faker->text,
    ];
});
