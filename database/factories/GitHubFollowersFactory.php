<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\dashboard\GitHub\Models\Users;
use App\dashboard\GitHub\Models\Followers;

$factory->define(Followers::class, function () {
    return [
        'user_id' => function () {
            return factory(\App\dashboard\Users\Models\Users::class)->create()->id;
        },
        'follower_id' => function () {
            return factory(Users::class)->create()->id;
        },
    ];
});
