<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use Faker\Generator as Faker;
use App\dashboard\Projects\Models\Projects;

$factory->define(Projects::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'full_name' => $faker->name,
        'description' => $faker->text,
    ];
});
