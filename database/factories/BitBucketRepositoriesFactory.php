<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use Faker\Generator as Faker;
use App\dashboard\BitBucket\Models\Repositories;

$factory->define(Repositories::class, function (Faker $faker) {
    return [
        'uuid' => $faker->uuid,
        'name' => $faker->name,
        'full_name' => $faker->name,
        'html_url' => $faker->url,
    ];
});
