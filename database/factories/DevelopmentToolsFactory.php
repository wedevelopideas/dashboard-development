<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use Faker\Generator as Faker;
use App\dashboard\Tools\Models\DevelopmentTools;

$factory->define(DevelopmentTools::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'full_name' => $faker->name,
        'tag_name' => $faker->name,
        'html_url' => $faker->url,
        'description' => $faker->text,
        'homepage' => $faker->url,
    ];
});
