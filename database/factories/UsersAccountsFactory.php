<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use Illuminate\Support\Str;
use Faker\Generator as Faker;
use App\dashboard\Users\Models\UsersAccounts;

$factory->define(UsersAccounts::class, function (Faker $faker) {
    $provider = $faker->randomElement(['bitbucket', 'github']);

    return [
        'user_id' => random_int(1, 100000),
        'provider_id' => random_int(1, 100000),
        'provider' => $provider,
        'name' => $faker->name,
        'username' => $faker->userName,
        'avatar' => $faker->imageUrl(),
        'html_url' => $faker->url,
        'token' => Str::random(10),
        'refresh_token' => Str::random(10),
    ];
});
