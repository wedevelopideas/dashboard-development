<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use Faker\Generator as Faker;
use App\dashboard\GitHub\Models\Users;

$factory->define(Users::class, function (Faker $faker) {
    return [
        'github_id' => random_int(1, 100000),
        'name' => $faker->name,
        'username' => $faker->userName,
        'avatar_url' => $faker->imageUrl(),
        'html_url' => $faker->url,
        'location' => $faker->city,
    ];
});
