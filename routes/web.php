<?php

use Illuminate\Support\Facades\Route;

Route::get('', ['uses' => 'DashboardController@dashboard', 'as' => 'dashboard']);

Route::group(['namespace' => 'Auth'], function () {
    Route::get('login', ['uses' => 'LoginController@showForm', 'as' => 'login']);
    Route::post('login', ['uses' => 'LoginController@login']);
    Route::post('logout', ['uses' => 'LoginController@logout', 'as' => 'logout']);

    Route::get('login/bitbucket', ['uses' => 'BitbucketController@redirectToProvider', 'as' => 'login.bitbucket']);
    Route::get('login/bitbucket/callback', 'BitbucketController@handleProviderCallback');
    Route::get('login/github', ['uses' => 'GitHubController@redirectToProvider', 'as' => 'login.github']);
    Route::get('login/github/callback', 'GitHubController@handleProviderCallback');
});

Route::group(['middleware' => 'auth'], function () {
    Route::group(['prefix' => 'bitbucket'], function () {
        Route::get('', ['uses' => 'BitBucketController@index', 'as' => 'bitbucket']);
    });

    Route::group(['prefix' => 'github'], function () {
        Route::get('', ['uses' => 'GitHubController@index', 'as' => 'github']);
    });

    Route::group(['prefix' => 'projects'], function () {
        Route::get('', ['uses' => 'ProjectsController@index', 'as' => 'projects']);
        Route::get('{full_name}', ['uses' => 'ProjectsController@view', 'as' => 'projects.view']);
    });

    Route::get('{user_name}', ['uses' => 'UsersController@profile', 'as' => 'user']);
});
