<?php

namespace App\Http\Controllers;

use Illuminate\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Contracts\View\Factory;
use App\dashboard\GitHub\Repositories\FollowersRepository;
use App\dashboard\BitBucket\Repositories\RepositoriesRepository;
use App\dashboard\Tools\Repositories\DevelopmentToolsRepository;

class DashboardController extends Controller
{
    /**
     * @var DevelopmentToolsRepository
     */
    private $developmentToolsRepository;

    /**
     * @var FollowersRepository
     */
    private $followersRepository;

    /**
     * @var RepositoriesRepository
     */
    private $repositoriesRepository;

    /**
     * DashboardController constructor.
     * @param  DevelopmentToolsRepository  $developmentToolsRepository
     * @param  FollowersRepository  $followersRepository
     * @param  RepositoriesRepository  $repositoriesRepository
     */
    public function __construct(
        DevelopmentToolsRepository $developmentToolsRepository,
        FollowersRepository $followersRepository,
        RepositoriesRepository $repositoriesRepository
    ) {
        $this->middleware('auth');
        $this->developmentToolsRepository = $developmentToolsRepository;
        $this->followersRepository = $followersRepository;
        $this->repositoriesRepository = $repositoriesRepository;
    }

    /**
     * Shows the dashboard of the user.
     *
     * @return Factory|RedirectResponse|View
     */
    public function dashboard()
    {
        if (! auth()->check()) {
            return redirect()
                ->route('login');
        }

        $github_id = (! isset(auth()->user()->github()->first()->provider_id)) ?: auth()->user()->github()->first()->provider_id;

        $followings = $this->followersRepository->getFollowing($github_id);
        $repositories = $this->repositoriesRepository->getAllDashboard();
        $tools = $this->developmentToolsRepository->getAll();

        return view('dashboard')
            ->with('followings', $followings)
            ->with('repositories', $repositories)
            ->with('tools', $tools);
    }
}
