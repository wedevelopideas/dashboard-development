<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\RedirectResponse;
use Laravel\Socialite\Facades\Socialite;
use App\dashboard\Users\Models\UsersAccounts;

class BitbucketController extends Controller
{
    /**
     * Redirect the user to the BitBucket authentication page.
     *
     * @return mixed
     */
    public function redirectToProvider()
    {
        return Socialite::with('bitbucket')->redirect();
    }

    /**
     * Obtain the user information from GitHub.
     *
     * @return RedirectResponse
     */
    public function handleProviderCallback()
    {
        $user = Socialite::driver('bitbucket')->user();

        $data = app('Bitbucket')->user($user);

        $authUser = $this->findOrCreateUser($data);

        auth()->loginUsingId($authUser->id, true);

        return redirect()
            ->route('dashboard');
    }

    /**
     * Return user if exists.
     *
     * @param  array  $bitbucketUser
     * @return mixed
     */
    private function findOrCreateUser(array $bitbucketUser)
    {
        if ($authUser = UsersAccounts::where('provider_id', $bitbucketUser['provider_id'])->first()) {
            UsersAccounts::where('provider_id', $bitbucketUser['provider_id'])->update([
                'token' => $bitbucketUser['token'],
                'refresh_token' => $bitbucketUser['refresh_token'],
            ]);

            return $authUser->user;
        } else {
            $user = UsersAccounts::create([
                'user_id' => auth()->id(),
                'provider_id' => $bitbucketUser['provider_id'],
                'provider' => $bitbucketUser['provider'],
                'name' => $bitbucketUser['name'],
                'username' => $bitbucketUser['username'],
                'avatar' => $bitbucketUser['avatar'],
                'html_url' => $bitbucketUser['html_url'],
                'token' => $bitbucketUser['token'],
                'refresh_token' => $bitbucketUser['refresh_token'],
            ]);

            return $user;
        }
    }
}
