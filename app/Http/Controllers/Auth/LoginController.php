<?php

namespace App\Http\Controllers\Auth;

use Illuminate\View\View;
use App\Http\Controllers\Controller;
use Illuminate\Http\RedirectResponse;
use Illuminate\Contracts\View\Factory;
use App\dashboard\Users\Requests\LoginRequest;

class LoginController extends Controller
{
    /**
     * Shows the login form.
     *
     * @return Factory|View
     */
    public function showForm()
    {
        return view('auth.login');
    }

    /**
     * Handles the login request.
     *
     * @param LoginRequest $request
     * @return RedirectResponse
     */
    public function login(LoginRequest $request): RedirectResponse
    {
        $details = $request->only('email', 'password');

        if (auth()->attempt($details)) {
            return redirect()
                ->route('dashboard');
        }

        return redirect()
            ->route('login')
            ->with('error', trans('errors.login_failed'));
    }

    /**
     * Handles the logout request.
     *
     * @return RedirectResponse
     */
    public function logout(): RedirectResponse
    {
        auth()->logout();

        return redirect()
            ->route('login');
    }
}
