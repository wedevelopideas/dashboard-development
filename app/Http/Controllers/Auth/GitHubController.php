<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\RedirectResponse;
use Laravel\Socialite\Facades\Socialite;
use App\dashboard\Users\Models\UsersAccounts;

class GitHubController extends Controller
{
    /**
     * Redirect the user to the GitHub authentication page.
     *
     * @return mixed
     */
    public function redirectToProvider()
    {
        return Socialite::with('github')->scopes([
            'repo',
            'notifications',
            'read:org'
        ])->redirect();
    }

    /**
     * Obtain the user information from GitHub.
     *
     * @return RedirectResponse
     */
    public function handleProviderCallback()
    {
        $user = Socialite::driver('github')->user();

        $data = app('GitHub')->user($user);

        $authUser = $this->findOrCreateUser($data);

        auth()->loginUsingId($authUser->id, true);

        return redirect()
            ->route('dashboard');
    }

    /**
     * Return user if exists.
     *
     * @param  array  $githubUser
     * @return mixed
     */
    private function findOrCreateUser(array $githubUser)
    {
        if ($authUser = UsersAccounts::where('provider_id', $githubUser['provider_id'])->first()) {
            UsersAccounts::where('provider_id', $githubUser['provider_id'])->update([
                'token' => $githubUser['token'],
                'refresh_token' => $githubUser['refresh_token'],
            ]);

            return $authUser->user;
        } else {
            $user = UsersAccounts::create([
                'user_id' => auth()->id(),
                'provider_id' => $githubUser['provider_id'],
                'provider' => $githubUser['provider'],
                'name' => $githubUser['name'],
                'username' => $githubUser['username'],
                'avatar' => $githubUser['avatar'],
                'html_url' => $githubUser['html_url'],
                'token' => $githubUser['token'],
                'refresh_token' => $githubUser['refresh_token'],
            ]);

            return $user;
        }
    }
}
