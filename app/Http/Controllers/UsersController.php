<?php

namespace App\Http\Controllers;

use Illuminate\View\View;
use Illuminate\Contracts\View\Factory;
use App\dashboard\Users\Repositories\UsersRepository;
use App\dashboard\Users\Repositories\UsersAccountsRepository;

class UsersController extends Controller
{
    /**
     * @var UsersRepository
     */
    private $usersRepository;

    /**
     * @var UsersAccountsRepository
     */
    private $usersAccountsRepository;

    /**
     * UsersController constructor.
     * @param  UsersRepository  $usersRepository
     * @param  UsersAccountsRepository  $usersAccountsRepository
     */
    public function __construct(
        UsersAccountsRepository $usersAccountsRepository,
        UsersRepository $usersRepository
    ) {
        $this->usersRepository = $usersRepository;
        $this->usersAccountsRepository = $usersAccountsRepository;
    }

    /**
     * Shows the user profile.
     *
     * @param  string  $user_name
     * @return Factory|View
     */
    public function profile(string $user_name)
    {
        $user = $this->usersRepository->getUserByUserName($user_name);

        if (! $user) {
            abort(404);
        }

        $accounts = $this->usersAccountsRepository->getProvidersByUserID($user->id);

        return view('users.profile')
            ->with('accounts', $accounts)
            ->with('user', $user);
    }
}
