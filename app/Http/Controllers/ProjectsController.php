<?php

namespace App\Http\Controllers;

use Illuminate\View\View;
use Illuminate\Contracts\View\Factory;
use App\dashboard\Projects\Repositories\ProjectsRepository;
use App\dashboard\Projects\Repositories\ProjectsMembersRepository;

class ProjectsController extends Controller
{
    /**
     * @var ProjectsRepository
     */
    private $projectsRepository;

    /**
     * @var ProjectsMembersRepository
     */
    private $projectsMembersRepository;

    /**
     * ProjectsController constructor.
     * @param  ProjectsRepository  $projectsRepository
     * @param  ProjectsMembersRepository  $projectsMembersRepository
     */
    public function __construct(
        ProjectsRepository $projectsRepository,
        ProjectsMembersRepository $projectsMembersRepository
    ) {
        $this->projectsRepository = $projectsRepository;
        $this->projectsMembersRepository = $projectsMembersRepository;
    }

    /**
     * Shows all projects.
     *
     * @return Factory|View
     */
    public function index()
    {
        $projects = $this->projectsRepository->getAll();

        return view('projects.index')
            ->with('projects', $projects);
    }

    /**
     * Shows the project.
     *
     * @param  string  $full_name
     * @return Factory|View
     */
    public function view(string $full_name)
    {
        $project = $this->projectsRepository->getProjectByFullName($full_name);

        $members = $this->projectsMembersRepository->getAllProjectMembersByProjectId($project->id);

        return view('projects.view')
            ->with('members', $members)
            ->with('project', $project);
    }
}
