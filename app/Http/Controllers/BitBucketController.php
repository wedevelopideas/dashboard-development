<?php

namespace App\Http\Controllers;

use Illuminate\View\View;
use Illuminate\Contracts\View\Factory;
use App\dashboard\BitBucket\Repositories\RepositoriesRepository;

class BitBucketController extends Controller
{
    /**
     * @var RepositoriesRepository
     */
    private $repositoriesRepository;

    /**
     * BitBucketController constructor.
     * @param RepositoriesRepository $repositoriesRepository
     */
    public function __construct(RepositoriesRepository $repositoriesRepository)
    {
        $this->repositoriesRepository = $repositoriesRepository;
    }

    /**
     * Shows BitBucket index.
     *
     * @return Factory|View
     */
    public function index()
    {
        $repositories = $this->repositoriesRepository->getAll();

        return view('bitbucket')
            ->with('repositories', $repositories);
    }
}
