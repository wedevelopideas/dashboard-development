<?php

namespace App\Http\Controllers;

use Illuminate\View\View;
use Illuminate\Contracts\View\Factory;
use App\dashboard\GitHub\Repositories\FollowersRepository;
use App\dashboard\Users\Repositories\UsersAccountsRepository;

class GitHubController extends Controller
{
    /**
     * @var FollowersRepository
     */
    private $followersRepository;

    /**
     * @var UsersAccountsRepository
     */
    private $usersAccountsRepository;

    /**
     * GitHubController constructor.
     * @param  FollowersRepository  $followersRepository
     * @param  UsersAccountsRepository  $usersAccountsRepository
     */
    public function __construct(
        FollowersRepository $followersRepository,
        UsersAccountsRepository $usersAccountsRepository
    ) {
        $this->followersRepository = $followersRepository;
        $this->usersAccountsRepository = $usersAccountsRepository;
    }

    /**
     * Shows GitHub index.
     *
     * @return Factory|View
     */
    public function index()
    {
        $check = $this->usersAccountsRepository->checkProviderByUserID(auth()->id(), 'github');

        if (! $check) {
            abort(404);
        }

        $users = $this->followersRepository->getFollowing((int) $check->provider_id);

        return view('github')
            ->with('users', $users);
    }
}
