<?php

namespace App\dashboard\Users\Models;

use Illuminate\Support\Carbon;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Foundation\Auth\User as Authenticatable;

/**
 * Class Users.
 *
 * @property int $id
 * @property string $email
 * @property string $password
 * @property string $first_name
 * @property string $last_name
 * @property string $display_name
 * @property string $user_name
 * @property string $avatar
 * @property int $github_id
 * @property string|null $remember_token
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 */
class Users extends Authenticatable
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'email',
        'password',
        'first_name',
        'last_name',
        'display_name',
        'user_name',
        'avatar',
        'github_id',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * Providers relationship.
     *
     * @return HasMany
     */
    public function providers()
    {
        return $this->hasMany(UsersAccounts::class, 'user_id', 'id')
            ->orderBy('provider', 'ASC');
    }

    /**
     * Checks if BitBucket relationship exists.
     *
     * @return HasMany
     */
    public function bitbucket()
    {
        return $this->providers()
            ->where('provider', 'bitbucket');
    }

    /**
     * Checks if GitHub relationship exists.
     *
     * @return HasMany
     */
    public function github()
    {
        return $this->providers()
            ->where('provider', 'github');
    }
}
