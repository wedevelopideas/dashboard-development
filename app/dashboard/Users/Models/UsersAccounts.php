<?php

namespace App\dashboard\Users\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * Class UsersAccounts.
 *
 * @property int $user_id
 * @property string $provider_id
 * @property string $provider
 * @property string $name
 * @property string $username
 * @property string $avatar
 * @property string $html_url
 * @property string $token
 * @property string $refresh_token
 */
class UsersAccounts extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
        'provider_id',
        'provider',
        'name',
        'username',
        'avatar',
        'html_url',
        'token',
        'refresh_token',
    ];

    /**
     * Relationship for users.
     *
     * @return BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(Users::class, 'user_id', 'id');
    }
}
