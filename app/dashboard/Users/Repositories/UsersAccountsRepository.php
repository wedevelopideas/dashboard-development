<?php

namespace App\dashboard\Users\Repositories;

use App\dashboard\Users\Models\UsersAccounts;

class UsersAccountsRepository
{
    /**
     * @var UsersAccounts
     */
    private $usersAccounts;

    /**
     * UsersAccountsRepository constructor.
     * @param UsersAccounts $usersAccounts
     */
    public function __construct(UsersAccounts $usersAccounts)
    {
        $this->usersAccounts = $usersAccounts;
    }

    /**
     * Check if user is available for provider.
     *
     * @param  int  $user_id
     * @param  string  $provider
     * @return mixed
     */
    public function checkProviderByUserID(int $user_id, string $provider)
    {
        return $this->usersAccounts
            ->where('user_id', $user_id)
            ->where('provider', $provider)
            ->first();
    }

    /**
     * Get all providers by user id.
     *
     * @param  int  $user_id
     * @return mixed
     */
    public function getProvidersByUserID(int $user_id)
    {
        return $this->usersAccounts
            ->where('user_id', $user_id)
            ->whereNotNull('user_id')
            ->orderBy('provider', 'ASC')
            ->get();
    }
}
