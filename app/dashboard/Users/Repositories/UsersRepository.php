<?php

namespace App\dashboard\Users\Repositories;

use App\dashboard\Users\Models\Users;

class UsersRepository
{
    /**
     * @var Users
     */
    private $users;

    /**
     * UsersRepository constructor.
     * @param Users $users
     */
    public function __construct(Users $users)
    {
        $this->users = $users;
    }

    /**
     * Get the user by its username.
     *
     * @param  string  $user_name
     * @return mixed
     */
    public function getUserByUserName(string $user_name)
    {
        return $this->users
            ->where('user_name', $user_name)
            ->first();
    }
}
