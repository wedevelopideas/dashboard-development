<?php

namespace App\dashboard\Projects\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Projects.
 *
 * @property int $id
 * @property string $name
 * @property string $full_name
 * @property string $description
 */
class Projects extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'full_name',
        'description',
    ];

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;
}
