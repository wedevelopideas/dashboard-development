<?php

namespace App\dashboard\Projects\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class ProjectsMembers.
 *
 * @property int $id
 * @property int $project_id
 * @property int $user_id
 * @property string $type
 */
class ProjectsMembers extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'project_id',
        'user_id',
        'type',
    ];
}
