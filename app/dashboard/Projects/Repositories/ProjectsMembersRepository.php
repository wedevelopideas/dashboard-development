<?php

namespace App\dashboard\Projects\Repositories;

use App\dashboard\Projects\Models\ProjectsMembers;

class ProjectsMembersRepository
{
    /**
     * @var ProjectsMembers
     */
    private $projectsMembers;

    /**
     * ProjectsMembersRepository constructor.
     * @param ProjectsMembers $projectsMembers
     */
    public function __construct(ProjectsMembers $projectsMembers)
    {
        $this->projectsMembers = $projectsMembers;
    }

    /**
     * Get all project members by project id.
     *
     * @param  int  $project_id
     * @return mixed
     */
    public function getAllProjectMembersByProjectId(int $project_id)
    {
        return $this->projectsMembers
            ->join('users', 'users.id', '=', 'projects_members.user_id')
            ->where('projects_members.project_id', $project_id)
            ->orderBy('users.last_name', 'ASC')
            ->orderBy('users.first_name', 'ASC')
            ->get();
    }
}
