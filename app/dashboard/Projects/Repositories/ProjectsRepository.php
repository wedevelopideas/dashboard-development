<?php

namespace App\dashboard\Projects\Repositories;

use App\dashboard\Projects\Models\Projects;

class ProjectsRepository
{
    /**
     * @var Projects
     */
    private $projects;

    /**
     * ProjectsRepository constructor.
     * @param Projects $projects
     */
    public function __construct(Projects $projects)
    {
        $this->projects = $projects;
    }

    /**
     * Get all projects.
     *
     * @return mixed
     */
    public function getAll()
    {
        return $this->projects
            ->orderBy('full_name', 'ASC')
            ->get();
    }

    /**
     * Get the project by the full name.
     *
     * @param  string  $full_name
     * @return mixed
     */
    public function getProjectByFullName(string $full_name)
    {
        return $this->projects
            ->where('full_name', $full_name)
            ->firstOrFail();
    }
}
