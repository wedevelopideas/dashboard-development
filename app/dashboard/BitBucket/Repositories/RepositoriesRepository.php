<?php

namespace App\dashboard\BitBucket\Repositories;

use App\dashboard\BitBucket\Models\Repositories;

class RepositoriesRepository
{
    /**
     * @var Repositories
     */
    private $repositories;

    /**
     * RepositoriesRepository constructor.
     * @param Repositories $repositories
     */
    public function __construct(Repositories $repositories)
    {
        $this->repositories = $repositories;
    }

    /**
     * Check BitBucket repository.
     *
     * @param string $uuid
     * @return mixed
     */
    public function checkRepositories(string $uuid)
    {
        return $this->repositories
            ->where('uuid', $uuid)
            ->first();
    }

    /**
     * Create BitBucket repository.
     *
     * @param array $data
     */
    public function createRepositories(array $data): void
    {
        $repository = new Repositories();
        $repository->uuid = $data['uuid'];
        $repository->name = $data['name'];
        $repository->full_name = $data['full_name'];
        $repository->html_url = $data['html_url'];
        $repository->created_at = $data['created_at'];
        $repository->updated_at = $data['updated_at'];
        $repository->save();
    }

    /**
     * Get all BitBucket Repositories.
     *
     * @return mixed
     */
    public function getAll()
    {
        return $this->repositories
            ->orderBy('name', 'ASC')
            ->get();
    }

    /**
     * Get all BitBucket Repositories.
     *
     * @return mixed
     */
    public function getAllDashboard()
    {
        return $this->repositories
            ->orderBy('name', 'ASC')
            ->take(10)
            ->get();
    }

    /**
     * Update BitBucket repository.
     *
     * @param array $data
     */
    public function updateRepositories(array $data): void
    {
        $repository = Repositories::where('uuid', $data['uuid'])->first();

        if ($repository) {
            $repository->name = $data['name'];
            $repository->full_name = $data['full_name'];
            $repository->html_url = $data['html_url'];
            $repository->created_at = $data['created_at'];
            $repository->updated_at = $data['updated_at'];
            $repository->save();
        }
    }
}
