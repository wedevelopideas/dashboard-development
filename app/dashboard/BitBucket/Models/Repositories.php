<?php

namespace App\dashboard\BitBucket\Models;

use Illuminate\Support\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Repositories.
 *
 * @property int $id
 * @property string $uuid
 * @property string $name
 * @property string $full_name
 * @property string $html_url
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 */
class Repositories extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'bitbucket_repositories';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'uuid',
        'name',
        'full_name',
        'html_url',
    ];
}
