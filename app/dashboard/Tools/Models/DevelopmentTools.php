<?php

namespace App\dashboard\Tools\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class DevelopmentTools.
 *
 * @property int $id
 * @property string $name
 * @property string $full_name
 * @property string $tag_name
 * @property string $html_url
 * @property string $description
 * @property string $homepage
 */
class DevelopmentTools extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'development_tools';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'full_name',
        'tag_name',
        'html_url',
        'description',
        'homepage',
    ];

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;
}
