<?php

namespace App\dashboard\Tools\Repositories;

use App\dashboard\Tools\Models\DevelopmentTools;

class DevelopmentToolsRepository
{
    /**
     * @var DevelopmentTools
     */
    private $developmentTools;

    /**
     * DevelopmentToolsRepository constructor.
     * @param DevelopmentTools $developmentTools
     */
    public function __construct(DevelopmentTools $developmentTools)
    {
        $this->developmentTools = $developmentTools;
    }

    /**
     * Get all development tools.
     *
     * @return mixed
     */
    public function getAll()
    {
        return $this
            ->developmentTools
            ->orderBy('full_name', 'ASC')
            ->get();
    }

    /**
     * Update latest release of GitHub repositories.
     *
     * @param array $data
     */
    public function updateRelease(array $data)
    {
        $release = DevelopmentTools::find($data['id']);

        if ($release) {
            $release->tag_name = $data['tag_name'];
            $release->save();
        }
    }
}
