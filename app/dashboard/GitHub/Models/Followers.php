<?php

namespace App\dashboard\GitHub\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Followers.
 *
 * @property int $user_id
 * @property int $follower_id
 * @property mixed name
 * @property mixed username
 */
class Followers extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'github_followers';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
        'follower_id',
    ];

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * Sets the GitHub name.
     *
     * @return mixed
     */
    public function GitHubName()
    {
        if ($this->name) {
            return $this->name;
        }

        return $this->username;
    }
}
