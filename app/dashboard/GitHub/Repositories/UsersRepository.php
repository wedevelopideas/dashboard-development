<?php

namespace App\dashboard\GitHub\Repositories;

use App\dashboard\GitHub\Models\Users;

class UsersRepository
{
    /**
     * @var Users
     */
    private $users;

    /**
     * UsersRepository constructor.
     * @param Users $users
     */
    public function __construct(Users $users)
    {
        $this->users = $users;
    }

    /**
     * Get all users.
     *
     * @return mixed
     */
    public function getAll()
    {
        return $this->users
            ->orderBy('username', 'ASC')
            ->get();
    }

    /**
     * Update GitHub user.
     *
     * @param array $data
     */
    public function updateUser(array $data)
    {
        $user = Users::where('github_id', $data['github_id'])->first();

        if ($user) {
            $user->name = $data['name'];
            $user->username = $data['username'];
            $user->avatar_url = $data['avatar_url'];
            $user->html_url = $data['html_url'];
            $user->location = $data['location'];
            $user->created_at = $data['created_at'];
            $user->updated_at = $data['updated_at'];
            $user->save();
        }
    }
}
