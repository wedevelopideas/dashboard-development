<?php

namespace App\dashboard\GitHub\Repositories;

use App\dashboard\GitHub\Models\Followers;

class FollowersRepository
{
    /**
     * @var Followers
     */
    private $followers;

    /**
     * FollowersRepository constructor.
     * @param Followers $followers
     */
    public function __construct(Followers $followers)
    {
        $this->followers = $followers;
    }

    /**
     * Get all followings.
     *
     * @param int $user_id
     * @return mixed
     */
    public function getFollowing(int $user_id)
    {
        return $this->followers
            ->join('github_users', 'github_followers.follower_id', '=', 'github_users.github_id')
            ->where('github_followers.user_id', $user_id)
            ->orderBy('github_users.username', 'ASC')
            ->get();
    }
}
