<?php

namespace App\Providers;

use App\Services\GitHub;
use App\Services\Bitbucket;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        // Bitbucket service
        $this->app->singleton('Bitbucket', function () {
            return new Bitbucket();
        });

        // GitHub service
        $this->app->singleton('GitHub', function () {
            return new GitHub();
        });
    }
}
