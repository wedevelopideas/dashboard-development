<?php

namespace App\Console\Commands;

use GuzzleHttp\Client;
use Illuminate\Console\Command;
use GuzzleHttp\Exception\GuzzleException;
use App\dashboard\Tools\Repositories\DevelopmentToolsRepository;

class FetchDevTools extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'github:devtools';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Fetch latest releases of repositories';

    /**
     * @var DevelopmentToolsRepository
     */
    private $developmentToolsRepository;

    /**
     * Create a new command instance.
     *
     * @param DevelopmentToolsRepository $developmentToolsRepository
     */
    public function __construct(DevelopmentToolsRepository $developmentToolsRepository)
    {
        parent::__construct();
        $this->developmentToolsRepository = $developmentToolsRepository;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     * @throws GuzzleException
     */
    public function handle()
    {
        $tools = $this->developmentToolsRepository->getAll();

        foreach ($tools as $tool) {
            $client = new Client();
            $request = $client->request('GET', 'https://api.github.com/repos/'.$tool->full_name.'/releases/latest', [
                'client_id' => config('services.github.client_id'),
                'client_secret' => config('services.github.client_secret'),
            ]);

            $response = $request->getBody()->getContents();
            $json_data = json_decode($response);

            if ($tool->tag_name !== $json_data->tag_name) {
                $data = [
                    'id' => $tool->id,
                    'tag_name' => $json_data->tag_name,
                ];

                $this->developmentToolsRepository->updateRelease($data);
            }
        }
    }
}
