<?php

namespace App\Console\Commands;

use Carbon\Carbon;
use GuzzleHttp\Client;
use Illuminate\Console\Command;
use GuzzleHttp\Exception\GuzzleException;
use App\dashboard\GitHub\Repositories\UsersRepository;

class FetchGitHubUsers extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'github:users';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Fetch GitHub Users';

    /**
     * @var UsersRepository
     */
    private $usersRepository;

    /**
     * Create a new command instance.
     *
     * @param UsersRepository $usersRepository
     */
    public function __construct(UsersRepository $usersRepository)
    {
        parent::__construct();
        $this->usersRepository = $usersRepository;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     * @throws GuzzleException
     */
    public function handle()
    {
        $users = $this->usersRepository->getAll();

        foreach ($users as $user) {
            $client = new Client();
            $request = $client->request('GET', 'https://api.github.com/users/'.$user->username, [
                'client_id' => config('services.github.client_id'),
                'client_secret' => config('services.github.client_secret'),
            ]);
            $response = $request->getBody()->getContents();
            $json_data = json_decode($response);

            $api = [
                'github_id' => $json_data->id,
                'name' => $json_data->name,
                'username' => $json_data->login,
                'avatar_url' => $json_data->avatar_url,
                'html_url' => $json_data->html_url,
                'location' => $json_data->location,
                'created_at' => Carbon::parse($json_data->created_at)->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::parse($json_data->updated_at)->format('Y-m-d H:i:s'),
            ];

            $this->usersRepository->updateUser($api);
        }
    }
}
