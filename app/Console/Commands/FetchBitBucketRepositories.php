<?php

namespace App\Console\Commands;

use Carbon\Carbon;
use GuzzleHttp\Client;
use Illuminate\Console\Command;
use GuzzleHttp\Exception\GuzzleException;
use App\dashboard\BitBucket\Repositories\RepositoriesRepository;

class FetchBitBucketRepositories extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'bitbucket:repositories';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Fetch BitBucket repositories';

    /**
     * @var RepositoriesRepository
     */
    private $repositoriesRepository;

    /**
     * Create a new command instance.
     *
     * @param RepositoriesRepository $repositoriesRepository
     */
    public function __construct(RepositoriesRepository $repositoriesRepository)
    {
        parent::__construct();
        $this->repositoriesRepository = $repositoriesRepository;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     * @throws GuzzleException
     */
    public function handle()
    {
        $totalRepositories = 22;
        $pages = ceil($totalRepositories / 10);

        for ($i = 1; $i <= $pages; $i++) {
            $client = new Client();
            $request = $client->request('GET', 'https://bitbucket.org/!api/2.0/repositories/wedevelopideas?page='.$i, [
                'clienti_id' => config('services.bitbucket.client_id'),
                'client_secret' => config('services.bitbucket.client_secret'),
            ]);
            $response = $request->getBody()->getContents();
            $json_data = json_decode($response);

            foreach ($json_data->values as $data) {
                $api = [
                    'uuid' => $data->uuid,
                    'name' => $data->name,
                    'full_name' => $data->full_name,
                    'html_url' => $data->links->html->href,
                    'created_at' => Carbon::parse($data->created_on)->format('Y-m-d H:i:s'),
                    'updated_at' => Carbon::parse($data->updated_on)->format('Y-m-d H:i:s'),
                ];

                $repository = $this->repositoriesRepository->checkRepositories($api['uuid']);

                if ($repository) {
                    $this->repositoriesRepository->updateRepositories($api);
                } else {
                    $this->repositoriesRepository->createRepositories($api);
                }
            }
        }
    }
}
