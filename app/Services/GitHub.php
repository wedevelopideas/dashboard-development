<?php

namespace App\Services;

class GitHub
{
    /**
     * Collection of GitHub user.
     *
     * @param  object  $data
     * @return array
     */
    public function user($data)
    {
        return [
            'provider_id' => $data->id,
            'provider' => 'github',
            'name' => $data->name,
            'username' => $data->nickname,
            'avatar' => $data->avatar,
            'html_url' => $data->user['html_url'],
            'token' => $data->token,
            'refresh_token' => $data->refreshToken,
        ];
    }
}
