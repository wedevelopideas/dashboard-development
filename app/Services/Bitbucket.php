<?php

namespace App\Services;

class Bitbucket
{
    /**
     * Collection of Bitbucket user.
     *
     * @param  object  $data
     * @return array
     */
    public function user($data)
    {
        return [
            'provider_id' => $data->id,
            'provider' => 'bitbucket',
            'name' => $data->name,
            'username' => $data->user['username'],
            'avatar' => $data->avatar,
            'html_url' => $data->user['links']['html']['href'],
            'token' => $data->token,
            'refresh_token' => $data->refreshToken,
        ];
    }
}
